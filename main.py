#!/usr/bin/env python3
#Command line version of Dr.Nim, made by skylineone044; Menu
# Version 1.1

import os

def main():
    os.system("clear")
    print(47 * "=" + "\n" + 20 * " " + "Dr.Nim\n" + 21 * " " + " v.2\n" + 47 *  "=")
    print("\n  There are 12 coins. \n  Each player must take eiter one, two or three, \n  and not more, and the player who \n  takes the last coin wins the match.")
    print("\n What do you want to do?")
    print("[1] - Play aganist computer")
    print("[2] - Play aganist another player")
    print("[5] - Exit game")
    
    def inp():
        try:
            global startanswer
            startanswer = int(input("Type your choice: "))
            if startanswer > 2 and startanswer != 5 or startanswer < 0:
                raise ValueError
        except ValueError:
            print("Your choice is invalid! Please try agin.")
            inp()
    inp()
    
    if startanswer == 5:
        exit()
    elif startanswer == 1:
        import singleplayer
        singleplayer.main()
    elif startanswer == 2:
        import multiplayer
        multiplayer.mainM()
    else:
        print("Error in choice resolution!")

if __name__ == "__main__":
    main()
