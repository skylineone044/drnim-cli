#!/usr/bin/env python3
#Command line version of Dr.Nim, made by skylineone044; Multiplayer
version = 1.1
import time, os   #Built in module imports

#Variables -----------
var_debug = True
wait = 0.2
winner = None
playertoken = "ONE"
global coin_untaken, coin_taken_player, coin_taken_player_two, has_next_turn
has_next_turn = "ONE"
coin_untaken = "O"
coin_taken_player_one = "1"
coin_taken_player_two = "2"
# --------------------

def debug(msg):
    if var_debug == True:
        print("[DEBUG]: " + str(msg))
    else:
        pass

def endfunc(playertoken):
    winner = playertoken
    draw(coins, coins_player_one, coins_player_two, "end", playertoken)


def newgame():
    os.system("clear")
    print("\n\n")
    print("New game started")
    # Newgame reset
    debug("Resetting lists")
    global coins, coins_player_one, coins_player_two
    coins = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
    coins_player_one = []
    coins_player_two = []
    debug(coins)
    debug(coins_player_one)
    debug(coins_player_two)

def draw(coins, coins_player_one, coins_player_two, has_next_turn, playertoken):
    debug("Drawing started")
    os.system("clear")
    print(47 * "=" + "\n" + 20 * " " + "Dr.Nim\n" + 21 * " " + str(version) + "\n" + 47 *  "=")
    print(str(coin_untaken) + ": Coin that is not taken")
    print(str(coin_taken_player_one) + ": Coin that has been taken by player one")
    print(str(coin_taken_player_two) + ": Coin that has been taken by player two")
    print(45 * "-")
    print("\n    ", end="")
    
    for i in range(12):
        if i in coins:
            print(coin_untaken, end="")
        elif i in coins_player_one:
            print(coin_taken_player_one, end="")
        elif i in coins_player_two:
            print(coin_taken_player_two, end="")
        else:
            debug("[001]:Drawing Error")
    if has_next_turn != "end":
        print("\n")
        print(" - PLAYER " + str(playertoken) + "'s turn - \n")
        print("What do you want to do??")
        print("[0] - Restart game")
        print("[1] - Take one coin")
        print("[2] - Take two coins")
        print("[3] - Take three coins")
        print("[4] - Go back to the main menu")
        print("[5] - Exit game")
        if has_next_turn == "ONE":
            logic(coins, coins_player_one, coins_player_two, "ONE")
        if has_next_turn == "TWO":
            logic(coins, coins_player_one, coins_player_two, "TWO")
    elif has_next_turn == "end":
        def endfunc():
            print("    Player " + str(playertoken) + " has won the game!")
            print("What do you want to do??")
            print("[0] - Restart game")
            print("[4] - Go back to the main menu")
            print("[5] - Exit game")
            global endanswer
            try:
                endanswer = int(input("Type your choice here: "))
            except ValueError:
                print("Your choice is invalid! Please try agin.")
                endfunc()
            if endanswer == 0:
                mainM()
            if endanswer == 5:
                exit()
            if endanswer == 4:
                import main
                main.main()
            else:
                print("Your answer is outside of permitted range! Please try agin.")
                endfunc()
        endfunc()

def logic(coins, coins_player, coins_computer, playertoken):
    debug("Logic started")
    def move(is_trying_agin):
        if is_trying_agin == False:
            try:
                global answer
                answer = int(input("Type your move: "))
                if answer > 5 or answer < 0 or answer > len(coins):
                    raise ValueError
            except ValueError:
                print("Your move is invalid! Please try agin!")
                logic(coins, coins_player, coins_computer, playertoken)
        elif is_trying_agin == True:
            print("Your move is invalid! Please try agin!")
            move(False)

    
    move(False)

    if answer == 4:
        import main
        main.main()
    if answer == 0:
        mainM()
    if answer == 5:
        exit()
    if answer == 1 or answer == 2 or answer == 3:
# Take logic ----------------------------------------------------
        try:
            for i in range(answer):
                if playertoken == "ONE":
                    coins_player_one.append(coins[-1])
                elif playertoken == "TWO":
                    coins_player_two.append(coins[-1])
                else:
                    debug("IdentityError")
                coins.pop()
        except IndexError:
            move(True)
        debug(coins)
        debug(coins_player_one)
        debug(coins_player_two)
        
        if 0 in coins_player_one:
            endfunc("ONE")
        elif 0 in coins_player_two:
            endfunc("TWO")
        
        if playertoken == "ONE":
            playertoken = "TWO"
            draw(coins, coins_player_one, coins_player_two, "TWO", "TWO")
            logic(coins, coins_player_one, coins_player_two, "TWO")
        elif playertoken == "TWO":
            playertoken = "ONE"
            draw(coins, coins_player_one, coins_player_two, "ONE", "ONE")
            logic(coins, coins_player_one, coins_player_two, "ONE")

# ---------------------------------------------------------------

def mainM():
    newgame()
    debug("Main thread started")
    draw(coins, coins_player_one, coins_player_two, "ONE", "ONE")

if __name__ == "__main__":
    import main
    main.main()
