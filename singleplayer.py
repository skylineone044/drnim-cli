#!/usr/bin/env python3
#Command line version of Dr.Nim, made by skylineone044; Singleplayer
version = 1.4
import time, os   #Built in module imports

#Variables -----------
var_debug = False
wait = 0.6
c_won = False

global coin_untaken, coin_taken_player, coin_taken_computer
coin_untaken = "O"
coin_taken_player = "#"
coin_taken_computer = "@"
# --------------------

def debug(msg):
    if var_debug == True:
        print("[DEBUG]: " + str(msg))
    else:
        pass

def end():
    debug("END")
    c_won = True

def newgame():
    os.system("clear")
    print("\n\n")
    print("New game started")
    # Newgame reset
    debug("Resetting lists")
    global coins, coins_player, coins_computer
    coins = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
    coins_player = []
    coins_computer = []
    debug(coins)
    debug(coins_player)
    debug(coins_computer)

def draw(coins, coins_player, coins_computer, has_next_turn):
    debug("Drawing started")
    os.system("clear")
    print(47 * "=" + "\n" + 20 * " " + "Dr.Nim\n" + 21 * " " + str(version) + "\n" + 47 *  "=")
    print(str(coin_untaken) + ": Coin that is not taken")
    print(str(coin_taken_player) + ": Coin that has been taken by the player")
    print(str(coin_taken_computer) + ": Coin that has been taken by the computer")
    print(45 * "-")
    print("\n    ", end="")
    
    for i in range(12):
        if i in coins:
            print(coin_untaken, end="")
        elif i in coins_player:
            print(coin_taken_player, end="")
        elif i in coins_computer:
            print(coin_taken_computer, end="")
        else:
            debug("[001]:Drawing Error")
    else:
        if has_next_turn != "end":
            print("\n")
            print("What do you want to do??")
            print("[0] - Restart game")
            print("[1] - Take one coin")
            print("[2] - Take two coins")
            print("[3] - Take three coins")
            print("[4] - Go back to the main menu")
            print("[5] - Exit game")
            if has_next_turn == "player":
                logic(coins, coins_player, coins_computer)
            if has_next_turn == "comp":
                logic_computer(coins, coins_player, coins_computer, wait, answer)
        elif has_next_turn == "end":
            def endfunc():
                print("    The Computer has won the game!")
                print("What do you want to do??")
                print("[0] - Restart game")
                print("[4] - Go back to the main menu")
                print("[5] - Exit game")
                global endanswer
                try:
                    endanswer = int(input("Type your choice here: "))
                except ValueError:
                    print("Your choice is invalid! Please try agin.")
                    endfunc()

                if endanswer == 0:
                    main()
                if endanswer == 5:
                    exit()
                if endanswer == 4:
                    import main
                    main.main()
                else:
                    print("Your answer is outside of permitted range! Please try agin.")
                    endfunc()
            endfunc()

def logic(coins, coins_player, coins_computer):
    debug("Logic started")
    try:
        global answer
        answer = int(input("Type your move: "))
        if answer > 5 or answer < 0:
            raise ValueError
    except ValueError:
        print("Your move is invalid! Please try agin!")
        logic(coins, coins_player, coins_computer)
    
    if answer == 4:
        import main
        main.main()
    if answer == 0:
        main()
    if answer == 5:
        exit()
    if answer == 1 or answer == 2 or answer == 3:
# Take logic ----------------------------------------------------
        for i in range(answer):
            coins_player.append(coins[-1])
            coins.pop()
        debug(coins)
        debug(coins_player)
        debug(coins_computer)
        draw(coins, coins_player, coins_computer, "comp")
        logic_computer(coins, coins_player, coins_computer, wait, answer)

def logic_computer(coins, coins_player, coins_computer, wait, answer):
    for i in range(1, 4):
       text = ("The computer is thinking" + "." * i)
       print(text, end="\r")
       time.sleep(wait)
    else:
        for i in range(4-answer):
            coins_computer.append(coins[-1])
            coins.pop()
        debug(coins)
        debug(coins_player)
        debug(coins_computer)
        if 0 in coins_computer:
            draw(coins, coins_player, coins_computer, "end")
        else:
            draw(coins, coins_player, coins_computer, "player")

# ---------------------------------------------------------------

def main():
    newgame()
    debug("Main thread started")
    draw(coins, coins_player, coins_computer, "player")

if __name__ == "__main__":
    import main
    main.main()
